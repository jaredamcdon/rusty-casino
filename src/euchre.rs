use std::collections::HashMap;
use rand::Rng;

pub fn run() {
    let mut suits_map = HashMap::new();
    suits_map.insert(100,"Clubs");
    suits_map.insert(200,"Diamonds");
    suits_map.insert(300,"Hearts");
    suits_map.insert(400,"Spades");

    let mut suits_list:[i16;4] = [100,200,300,400];

    let mut faces_map = HashMap::new();
    faces_map.insert(09,"9");
    faces_map.insert(10,"10");
    faces_map.insert(11,"Jack");
    faces_map.insert(12,"Queen");
    faces_map.insert(13,"King");
    faces_map.insert(14,"Ace");

    let mut faces_list:[i16;6] = [09,10,11,12,13,14];
    
    struct Deck{
        deck: Vec<i16>
    }
    impl Deck {
        pub fn new() -> Self {
            Deck {
                deck: Vec::new()
            }
        }
        // take suits_list and faces_list to create list of faces holding all cards with suit and face
        // value iterating through both lists and adding i and j
        fn init(&mut self, suits: &mut [i16], faces: &mut [i16]) {
            for i in suits.iter() {
                for j in faces.iter() {
                    self.deck.push(i+j);
                }
            }
        }
        fn shuffle(&mut self) {
            // .try_into().unwrap() -> attempts usize to i32 conversion with failure if not
            let deck_length : usize = self.deck.len();
            let mut rng = rand::thread_rng();
            let mut rng_value : usize;
            let mut rng_values : Vec<i16> = Vec::new();
            let iter_deck = &mut self.deck;
            for i in 0..deck_length {
                rng_value = rng.gen_range(0..(deck_length-i));
                rng_values.push(iter_deck[rng_value]);
                iter_deck.remove(rng_value);
                //rng_values.push(rng_value);
            }
            self.deck = rng_values;
        }
        fn deal(&mut self) -> Vec<Vec<i16>> {
            let mut rng = rand::thread_rng();
            let first_deal : usize = rng.gen_range(2..=3);
            let mut to_send : Vec<Vec<i16>> = vec![vec![];4];
            for deal_turn in 0..to_send.len() {
                for _i in 0..first_deal {
                    to_send[deal_turn].push(self.deck.pop().unwrap());
                }
            }
            for deal_turn in 0..to_send.len() {
                for _i in 0..(5-first_deal) {
                    to_send[deal_turn].push(self.deck.pop().unwrap());
                }
            }
            return to_send
        }
    }
    
    struct Player{
        hand: Vec<i16>
    }
    impl Player {
        pub fn new() -> Self {
            Player {
                hand: Vec::new()
            }
        }
    }

    struct Table{
        players: [Player;4],
        kitty: Vec<i16>,
        dealer: i8
    }
    impl Table {
        pub fn new() -> Self {
            Table{
                players: [
                    Player::new(),
                    Player::new(),
                    Player::new(),
                    Player::new()
                ],
                kitty: Vec::new(),
                dealer: 4
            }
        }
        fn init(&mut self, kitty_cards: Vec<i16>, mut player_cards: Vec<Vec<i16>>){
            self.kitty = kitty_cards;
            for mut player in &mut self.players{
                player.hand = player_cards.pop().unwrap();
            }
        }
        fn set_dealer(&mut self, mut dealer_deck: Vec<i16>) {
            let mut card_counter = 0;
            for _card in 0..dealer_deck.len(){
                let current: i16 = dealer_deck.pop().unwrap();
                if current == 111 || current == 411{
                    self.dealer = card_counter % 4;
                    println!("{:?}", current);
                    break;
                }
                card_counter+=1

            }
        }
        fn order_up(&mut self) {
                let top_decked = self.kitty[0];
        }
        fn change_trump(&mut self) {
            fn ask_player() -> bool {
                return true          
            }
        }
    }


    let mut new_deck = Deck::new();
    let mut new_table = Table::new();
    new_deck.init(&mut suits_list, &mut faces_list);
    println!("Unshuffled deck\t{:?}", new_deck.deck);
    new_deck.shuffle();
    println!("Shuffled deck\t{:?}", new_deck.deck);
    let to_players = new_deck.deal();
    new_table.init(new_deck.deck, to_players);
    println!("players[0] hand:{:?}", new_table.players[0].hand);
    println!("players[1] hand:{:?}", new_table.players[1].hand);
    println!("players[2] hand:{:?}", new_table.players[2].hand);
    println!("players[3] hand:{:?}", new_table.players[3].hand);
    println!("kitty: {:?}", new_table.kitty);
    println!("Top Kitty card: {:?}", new_table.kitty[0]);
    let mut dealer_deck = Deck::new();
    dealer_deck.init(&mut suits_list, &mut faces_list);
    dealer_deck.shuffle();
    new_table.set_dealer(dealer_deck.deck);
    println!("dealer: {:?}", new_table.dealer);
    /*
     * ask_player()
     *
    */
}
